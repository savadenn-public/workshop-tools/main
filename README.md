# Workshop Tools

General project of Workshop Tools suite

* [Latest online documentation](https://savadenn-public.gitlab.io/workshop-tools/workshop-planner/main/)
* [Getting started](https://savadenn-public.gitlab.io/workshop-tools/main/#getting-started)

## Contact

You may join us on our public [Matrix room](https://matrix.to/#/#workshop-tools:savadenn.ems.host)

## Authors and contributors

Time Manager is made by [Savadenn](https://www.savadenn.bzh) with the help of individual contributors.

The complete list can be found in the documentation of each [projects](https://gitlab.com/savadenn-public/workshop-tools).