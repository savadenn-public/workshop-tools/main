# Roles

`actor`{#actor}
`actors`{#actors alias-for=actor}

::: {def-for=actor}
Someone or something working for or on this system.
:::

`user`{#user}
`users`{#users alias-for=user}

::: {def-for=user}
A resource which represent an `::actor` in this system. It has secret credentials to ensure identity.
:::