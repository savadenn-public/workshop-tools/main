# Open-Source & License

Workshop Tools is licensed under [AGPL3.0](https://gitlab.com/savadenn-public/workshop-tools/main/-/raw/latest/LICENSE)