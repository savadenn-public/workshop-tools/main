# Development

## Getting started {#getting-started}

_You need our [docker localhost](https://gitlab.com/savadenn-public/ansible-roles/local-dev#docker-localhost) stack._

```console
git clone git@gitlab.com:savadenn-public/workshop-tools/main.git workshop-tools
cd workshop-tools
make prod
```

Connect to [https://workshop.docker.localhost](https://workshop.docker.localhost)

::: note
An admin user is created at first start

* email: bob@example.com
* pass: foo
:::

::: tip
You have successfully started a workshop complete environment using latest images.
:::

## Start to develop

1. Clone sources of one `::services-title | lower`, you want ot work on
2. Go into the project
3. Launch

    ```console
    make dev
    ```
   
4. Now, it's up to you to change everything! _Bon vent_[^gl]!

[^gl]: French for _Good luck_. It's literally _Good wind_ and come from sailing tradition.