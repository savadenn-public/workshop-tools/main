# Authors and contributors

Time Manager is made by [Savadenn](https://www.savadenn.bzh) with the help of individual contributors.

The complete list can be found in the documentation of each [projects](https://gitlab.com/savadenn-public/workshop-tools). For this project

For this documentation, they are listed [here](https://gitlab.com/savadenn-public/workshop-tools/main/-/graphs/latest).
