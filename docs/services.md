# Services {#services-title}

::: {def-for=auth}
`Workshop Authentication`{#auth} is a REST service for every aspect of authentication and `::user` resource.

* [Sources & project](https://gitlab.com/savadenn-public/workshop-tools/authentication)
* [Latest online documentation](https://gitlab.com/savadenn-public/workshop-tools/authentication)

:::

::: {def-for=api}
`Workshop API`{#api} is REST server service for planification resources.

* [Sources & project](https://gitlab.com/savadenn-public/workshop-tools/workshop-planner)
* [Latest online documentation](https://savadenn-public.gitlab.io/workshop-tools/workshop-planner)

:::

::: {def-for=pwa}
`Workshop PWA`{#pwa} is the Web UI service.

* [Sources & project](https://gitlab.com/savadenn-public/workshop-tools/workshop-pwa)
* [Latest online documentation](https://savadenn-public.gitlab.io/workshop-tools/workshop-pwa/)

:::

## Development only

| Service | Docs                                               | Endpoint |
|:--------|:---------------------------------------------------|:---------|
|`::pwa`  | [doc](https://docspwa-myworkwhop.docker.localhost) | [Client GUI](https://workshop.docker.localhost/pwa)|
|`::auth` | [doc](https://docsauth-workshop.docker.localhost)  | [Swagger](https://workshop.docker.localhost/auth/docs)|
|`::api`  | [doc](https://docsapi-workshop.docker.localhost)   | [Swagger](https://workshop.docker.localhost/api/docs)|
| MailDev |                                                    | [MailDev](https://mail-workshop.docker.localhost) |

